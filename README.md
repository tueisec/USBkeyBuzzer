# USBkeyBuzzer
A Gameshow Buzzer based on an XMC4500 Relax Lite Kit that enumerates as standard keyboard and works without special drivers.

![Completed USBkeyBuzzer](./complete.png "Completed USBkeyBuzzer")

## Building it up
### Software
Set up the toolchain for building projects for the XMC4500 under linux.
Confer the [toolchain.md](SW/toolchain.md) file for details.

Change the character that should be send to the computer once the button is pressed in line 65 of [main.c](SW/main.c) in the SW folder.
Compile and program the software to the Relax Lite Kit.
Note that the Relax Lite Kits have two micro USB connectors, one for programming on the smaller part of the board and one for applications next to the unpopulated ethernet jack.

If you now connect the board with the application USB port, you can already test the software by pressing the _Button 1_ that is already on the Relax Lite Kit.
Although the board is working at this point, to make the buzzer mechanically robust to survive a rough gaming experience and to make it easier for the players to hit the button, continue to mount the board into an industrial grade buzzer.

### Hardware
What you need:
* A buzzer. For the real gameshow feeling, choose a large button such as the ones used for emergency shutdown, in German they are called *Pilztaster*.
  It needs to have at least one normally-open (NO) contact.
  They can be quite expensive, but some online shops also have cheap (~30€) ones.
* An XMC4500 Relax Lite Kit (~12€)
* An A to right-angeled micro-B USB cable.
  Make sure the cable is long enough even if you play with three or four players and that the power wires are at least AWG24, better AWG20, *not AWG28*.
  The data lines will always be AWG28, which is fine. (~8€)
* Proper strain relief for the USB cable with bend protection and counter nut.
  It is probably hard to get one that is large enough to get the connectors through but can fit tight enough to hold the cable.
  Our recommended solution is to cut the cable shortly behind the microB connector, pass it through the strain relief and solder it together again. (~2€)
* A red and green LED with housing and for each an 82 Ohm series resistor to drive approx. 20 mA at 3.3 V. (~1€)
* A 3.3 V voltage regulator that is capable of providing 200 mA from as little as 4.3 V.
  The schematic and a PCB layout for such a regulator can be found in the HW folder. (~2€)

![Inside of the USBkeyBuzzer before attaching the USB cable](./inside.png "Inside of the USBkeyBuzzer before attaching the USB cable")

Build instructions:
1. Cut holes for the LEDs and the strain relief into the buzzer case and mount the strain relief
1. Solder the series resistors to the LEDs, then mount them into the buzzer case
1. Verify that your board has been flashed with the correct software and is working properly, then break off the debugger part so the board fits into the buzzer case.
   If you have a larger buzzer it is still good to remove the debugger, since the debugger draws almost the same current as the application processor and leaving it would double the power consumption of your buzzer.
   In case you later need to change the software on the board, you can reconnect the debugger with a 10 conductor ribbon cable and 2x5 pin connectors.
1. Solder up the voltage regulator using the [schematic](HW/vRegulator.sch.pdf) and [layout](HW/vRegulator.brd.pdf) in the HW folder.
   The layout is made to allow using a standard hole board instead of a custom PCB.
   Use standard 2.54mm pin headers and receptacles to connect it to the Relax Lite Kit.
   The rows of holes have a nice combination of 5 V, 3.3 V, and GND signals (in this order) so the voltage regulator can be directly attached to these pins.
   Please note that the voltage regulator is only necessary once the debugger is removed (as recommended).
1. Connect the LEDs and the button to the board using some small wires, confer the [wiring schematic](HW/wiring.sch.pdf) in the HW folder for details.
   Note that it is wise to first solder the cables to the board, then place it in the buzzer case, then solder the other ends to the LEDs and buzzer.
   If your buzzer has screw terminals, tin the ends of the wires that go into the terminals so their diameter increases and the screw terminals can hold them better.
1. Mount the USB cable and connect it to the application microB USB port.
   To fit it through the strain relief, cut the cable shortly behind the microB connector, pass the longer part through the strain relief and solder the cable together again.
   Make sure to properly insulate the wires of the cable and that the connection remains in the buzzer case and does not move into or to the outer side of the strain relief.
1. Mount the upper half of the buzzer and close the case.
   Your USBkeyBuzzer is now ready to use!

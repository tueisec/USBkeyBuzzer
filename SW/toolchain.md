# Installation #

## For Debian based systems (including Ubuntu, Kubuntu, etc) ##

1. Add the PPA from ARM for their `gcc` port and install the `gcc-arm-embedded` package:
```
   sudo add-apt-repository ppa:team-gcc-arm-embedded/ppa
   sudo apt-get update
   sudo apt-get install gcc-arm-embedded
```
2. Download the DEB Installer for the [J-Link Software and Documentation Pack](https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack) in the correct architecture (32-bit or 64-bit).
   Install `jlink` via
```
   sudo dpkg -i <filename you just downloaded>
```

3. Install the xmclib package you have received with this HowTo via
```
   sudo dpkg -i <filename of package>
```

## For arch based systems ##
1. Install `arm-none-eabi-gcc` and `arm-none-eabi-newlib` from official repository.
2. Install `jlink` meta package from AUR.
3. Confer step 3 of 'For other systems'

## For other systems ##

1. Install the toolchain
  * Download [gcc arm non-eabi toolchain](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads) for your architecture and extract to appropriate folder, e.g. `/opt/gcc-arm-none-eabi`.
  * Include the path to the new toolchain into the PATH environment variable, for example by adding `export PATH=$PATH:/opt/gcc-arm-none-eabi/gcc-arm-none-eabi-6-2017-q2-update/bin/` to your `~/.profile` or adding the beforementioned path to `/etc/environment` or whatever is the preferred way to do on your system.

2. Install the SEGGER programming tools
  * Download "J-Link Software and Documentation Pack" from [their website](https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack) in your preferred compression form and install them. (the .deb packet will install at `/opt/SEGGER`)

3. Install XMClib
  * Extract the contents of the .deb package into your root file system, so they end up in `/opt/XMClib`.

Make sure the folders are readable/executable by the users intended to use the toolchain.

### Test ###
Try compiling the given example_project.
1. Go to example_project
2. Run `make`.  
   If it doesn't work, recheck your installation process and try getting help [here](https://www.startpage.com).

# Usage #

1. Write your program as you are used to. Be aware of the specialties of embedded programming, e.g. your main.c must never terminate and gets no argc, argv.

2. Setup Makefile
  * Copy Makefile into project directory.
  * Adapt path in symbol `XMC_LIBDIR` if you chose another location during installation.
  * Choose a name for the linker description file in symbol `LDname`. What name is not important, but many people use the project's name.
  * If necessary, change remaining settings in symbol `SCFLAGS` according to your wishes.
  * Add filenames of code written by you to symbol `SRCS` and filenames of library code used in your project to symbol `LIBSRCS`. If you have to link precompiled libraries, specify them in symbol `LIBLNK`, e.g. for math operations add `-lm` there.

3. `make` will compile and link the program into an elf file, create a human readable lst file and print the size of the compiled program.

4. `make program` will download the compiled program onto the attached device, overwriting whatever program existed priorly.  
   It also resets and runs the device, so the new code is executed immediately.



# Debugging #

1. `make debug` will launch the SEGGER JLinkGDBServer and connect an instance of the `arm-none-eabi-gdb` in TUI mode to it.  
   If necessary, the project will be rebuilt and loaded onto the device, no prior programming required.  
   A breakpoint at the beginning of main() is placed automatically.

If you are unfamiliar with GDB or the TUI mode, go get a GDB quick reference card from e.g. [utexas.edu](https://users.ece.utexas.edu/~adnan/gdb-refcard.pdf).
GBD offers impressive features once you know how to use it.


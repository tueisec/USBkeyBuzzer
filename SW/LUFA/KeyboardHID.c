/*
             LUFA Library
     Copyright (C) Dean Camera, 2014.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/

/*
  Copyright 2014  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaims all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/
/*
  Copyright 2017  Tobias Fuchs
  Modified to use keyboard HID profile
*/
/*
  Copyright 2017  Florian Wilde
  Adapted to use within USBkeyBuzzer
*/

#include "KeyboardHID.h"
#include "german_keyboardCodes.h"
#include "system_XMC4500.h"
#ifdef LED_LOCKSTATUS
  #include <xmc_gpio.h>
#endif

// For comparison within the driver. I think we don't need it directly
static uint8_t PrevHIDReportBuffer[KEYBOARD_REPORT_SIZE];

USB_ClassInfo_HID_Device_t Keyboard_HID_Interface = {
	.Config = {
		.InterfaceNumber              = INTERFACE_ID_KeyboardHID,
		.ReportINEndpoint             = {
			.Address              = KEYBOARD_IN_EPADDR,
			.Size                 = KEYBOARD_REPORT_SIZE,
			.Banks                = 1,
		},
		.PrevReportINBuffer           = PrevHIDReportBuffer,
		.PrevReportINBufferSize       = sizeof(PrevHIDReportBuffer),
	},
};

uint8_t buffer[KEYBOARD_REPORT_SIZE];

/* USB runtime structure*/
XMC_USBD_t USB_runtime = {
	.usbd = USB0,
	.usbd_max_num_eps = XMC_USBD_MAX_NUM_EPS_6,
	.usbd_transfer_mode = XMC_USBD_USE_FIFO,
	.cb_xmc_device_event = USBD_SignalDeviceEventHandler,
	.cb_endpoint_event = USBD_SignalEndpointEvent_Handler
};

void USB0_0_IRQHandler(void)
{
	XMC_USBD_IRQHandler(&USB_runtime);
}

/*The function initializes the USB core layer and register call backs. */
void USB_Init(void)
{
#ifdef LED_LOCKSTATUS
  const XMC_GPIO_CONFIG_t LED_config = {
      .mode=XMC_GPIO_MODE_OUTPUT_PUSH_PULL,
      .output_level=XMC_GPIO_OUTPUT_LEVEL_LOW,
      .output_strength=XMC_GPIO_OUTPUT_STRENGTH_STRONG_SHARP_EDGE
  };
#endif
	USBD_Initialize(&USB_runtime);

	/* Interrupts configuration*/
	NVIC_SetPriority( USB0_0_IRQn,
	NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 63, 0) );
	NVIC_ClearPendingIRQ(USB0_0_IRQn);
	NVIC_EnableIRQ(USB0_0_IRQn);

	/* USB Connection*/
	USB_Attach();

#ifdef LED_LOCKSTATUS
  XMC_GPIO_Init(NUMLOCK_LED, &LED_config);
  XMC_GPIO_Init(CAPSLOCK_LED, &LED_config);
#endif
}

/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_(void)
{
}
/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
	//Device is ready!
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Reset(void)
{
	if(device.IsConfigured) {
		USB_Init();
		device.IsConfigured=0;
	}
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
	bool ConfigSuccess = true;

	USBD_SetEndpointBuffer(KEYBOARD_IN_EPADDR, buffer, KEYBOARD_REPORT_SIZE);

	ConfigSuccess &= HID_Device_ConfigureEndpoints(&Keyboard_HID_Interface);

	device.IsConfigured = ConfigSuccess;
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
	HID_Device_ProcessControlRequest(&Keyboard_HID_Interface);
}

// LUT to determine the keyboard report values from ascii character
const char sendable[][3] = {
	['A'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_A},
	['B'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_B},
	['C'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_C},
	['D'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_D},
	['E'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_E},
	['F'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_F},
	['G'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_G},
	['H'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_H},
	['I'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_I},
	['J'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_J},
	['K'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_K},
	['L'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_L},
	['M'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_M},
	['N'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_N},
	['O'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_O},
	['P'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_P},
	['Q'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_Q},
	['R'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_R},
	['S'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_S},
	['T'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_T},
	['U'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_U},
	['V'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_V},
	['W'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_W},
	['X'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_X},
	['Y'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_Y},
	['Z'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_Z},
	['a'] = {0, 0, GERMAN_KEYBOARD_SC_A},
	['b'] = {0, 0, GERMAN_KEYBOARD_SC_B},
	['c'] = {0, 0, GERMAN_KEYBOARD_SC_C},
	['d'] = {0, 0, GERMAN_KEYBOARD_SC_D},
	['e'] = {0, 0, GERMAN_KEYBOARD_SC_E},
	['f'] = {0, 0, GERMAN_KEYBOARD_SC_F},
	['g'] = {0, 0, GERMAN_KEYBOARD_SC_G},
	['h'] = {0, 0, GERMAN_KEYBOARD_SC_H},
	['i'] = {0, 0, GERMAN_KEYBOARD_SC_I},
	['j'] = {0, 0, GERMAN_KEYBOARD_SC_J},
	['k'] = {0, 0, GERMAN_KEYBOARD_SC_K},
	['l'] = {0, 0, GERMAN_KEYBOARD_SC_L},
	['m'] = {0, 0, GERMAN_KEYBOARD_SC_M},
	['n'] = {0, 0, GERMAN_KEYBOARD_SC_N},
	['o'] = {0, 0, GERMAN_KEYBOARD_SC_O},
	['p'] = {0, 0, GERMAN_KEYBOARD_SC_P},
	['q'] = {0, 0, GERMAN_KEYBOARD_SC_Q},
	['r'] = {0, 0, GERMAN_KEYBOARD_SC_R},
	['s'] = {0, 0, GERMAN_KEYBOARD_SC_S},
	['t'] = {0, 0, GERMAN_KEYBOARD_SC_T},
	['u'] = {0, 0, GERMAN_KEYBOARD_SC_U},
	['v'] = {0, 0, GERMAN_KEYBOARD_SC_V},
	['w'] = {0, 0, GERMAN_KEYBOARD_SC_W},
	['x'] = {0, 0, GERMAN_KEYBOARD_SC_X},
	['y'] = {0, 0, GERMAN_KEYBOARD_SC_Y},
	['z'] = {0, 0, GERMAN_KEYBOARD_SC_Z},
	['1'] = {0, 0, GERMAN_KEYBOARD_SC_1_AND_EXCLAMATION},
	['2'] = {0, 0, GERMAN_KEYBOARD_SC_2_AND_QUOTES},
	['3'] = {0, 0, GERMAN_KEYBOARD_SC_3_AND_PARAGRAPH},
	['4'] = {0, 0, GERMAN_KEYBOARD_SC_4_AND_DOLLAR},
	['5'] = {0, 0, GERMAN_KEYBOARD_SC_5_AND_PERCENTAGE},
	['6'] = {0, 0, GERMAN_KEYBOARD_SC_6_AND_AMPERSAND},
	['7'] = { 0, 0,
			 	GERMAN_KEYBOARD_SC_7_AND_SLASH_AND_OPENING_BRACE},
	['8'] = { 0, 0,
			  GERMAN_KEYBOARD_SC_8_AND_OPENING_PARENTHESIS_AND_OPENING_BRACKET},
	['9'] = { 0, 0,
			  GERMAN_KEYBOARD_SC_9_AND_CLOSING_PARENTHESIS_AND_CLOSING_BRACKET},
	['0'] = {0, 0, GERMAN_KEYBOARD_SC_0_AND_EQUAL_AND_CLOSING_BRACE},
	['!'] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
		 	  GERMAN_KEYBOARD_SC_1_AND_EXCLAMATION },
	['@'] = { HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
		 	  GERMAN_KEYBOARD_SC_Q },
	['#'] = { 0, 0, GERMAN_KEYBOARD_SC_HASHMARK_AND_APOSTROPHE},
	['\''] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			   GERMAN_KEYBOARD_SC_HASHMARK_AND_APOSTROPHE },
	['$'] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			  GERMAN_KEYBOARD_SC_4_AND_DOLLAR },
	['%'] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
		 	  GERMAN_KEYBOARD_SC_5_AND_PERCENTAGE },
	['&'] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
		 	  GERMAN_KEYBOARD_SC_6_AND_AMPERSAND },
	['('] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			  GERMAN_KEYBOARD_SC_8_AND_OPENING_PARENTHESIS_AND_OPENING_BRACKET},
	[')'] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
  			  GERMAN_KEYBOARD_SC_9_AND_CLOSING_PARENTHESIS_AND_CLOSING_BRACKET},
    ['='] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
		 	  GERMAN_KEYBOARD_SC_0_AND_EQUAL_AND_CLOSING_BRACE },
	['\"']= { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			  GERMAN_KEYBOARD_SC_2_AND_QUOTES },
	['/'] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
		 	  GERMAN_KEYBOARD_SC_7_AND_SLASH_AND_OPENING_BRACE},
	['['] = { HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			  GERMAN_KEYBOARD_SC_8_AND_OPENING_PARENTHESIS_AND_OPENING_BRACKET},
	[']'] = { HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			  GERMAN_KEYBOARD_SC_9_AND_CLOSING_PARENTHESIS_AND_CLOSING_BRACKET},
	['{'] = { HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
		      GERMAN_KEYBOARD_SC_7_AND_SLASH_AND_OPENING_BRACE },
	['}'] = { HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			  GERMAN_KEYBOARD_SC_0_AND_EQUAL_AND_CLOSING_BRACE },
	['?'] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			  GERMAN_KEYBOARD_SC_SHARP_S_AND_QUESTION_AND_BACKSLASH },
	['\\']= { HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			  GERMAN_KEYBOARD_SC_SHARP_S_AND_QUESTION_AND_BACKSLASH },
	['^'] = {0, 0, GERMAN_KEYBOARD_SC_CARET_AND_DEGREE},
    ['+'] = {0, 0, GERMAN_KEYBOARD_SC_PLUS_AND_ASTERISK_AND_TILDE},
	['*'] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
		 	  GERMAN_KEYBOARD_SC_PLUS_AND_ASTERISK_AND_TILDE },
	['~'] = { HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			  GERMAN_KEYBOARD_SC_PLUS_AND_ASTERISK_AND_TILDE },
    ['-'] = {0, 0, GERMAN_KEYBOARD_SC_MINUS_AND_UNDERSCORE },
    ['_'] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
		 	  GERMAN_KEYBOARD_SC_MINUS_AND_UNDERSCORE },
    [','] = {0, 0, GERMAN_KEYBOARD_SC_COMMA_AND_SEMICOLON},
    [';'] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			  GERMAN_KEYBOARD_SC_COMMA_AND_SEMICOLON },
	['.'] = {0, 0, GERMAN_KEYBOARD_SC_DOT_AND_COLON},
    [':'] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			  GERMAN_KEYBOARD_SC_DOT_AND_COLON },
	['<'] = {0, 0, GERMAN_KEYBOARD_SC_LESS_THAN_AND_GREATER_THAN_AND_PIPE},
	['>'] = { HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
		 	  GERMAN_KEYBOARD_SC_LESS_THAN_AND_GREATER_THAN_AND_PIPE },
	['|'] = { HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			  GERMAN_KEYBOARD_SC_LESS_THAN_AND_GREATER_THAN_AND_PIPE },
//	['\0']= {0, 0, GERMAN_KEYBOARD_SC_ENTER}, Avoid enter upon end of string
	['\n']= {0, 0, GERMAN_KEYBOARD_SC_ENTER},
	[' '] = {0, 0, GERMAN_KEYBOARD_SC_SPACE}
};

// string to be sent, updated by setString2send
char outstr[25] = { 0 };
size_t idxEnd = 0, idx = 0;

int setHIDoutstr(const char *str, size_t len) {
  if(isHIDBusy()) return 0;

  size_t ilen = strlen(str) + 1U;
  idxEnd = ilen < len ? ilen : len;

  if(idxEnd < sizeof(outstr)) {
    strncpy(outstr, str, idxEnd - 1U);
    outstr[idxEnd - 1U] = 0;
  } else {
    idxEnd = 0;
  }
  idx = 0;
  return idxEnd > 0;
}

int isHIDBusy(void) {
  return idx < idxEnd;
}

// Callback function called when a new HID report needs to be created
bool CALLBACK_HID_Device_CreateHIDReport(
							USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
							uint8_t* const ReportID,
							const uint8_t ReportType,
							void* ReportData,
							uint16_t* const ReportSize )
{
	USB_KeyboardReport_Data_t* report = (USB_KeyboardReport_Data_t *)ReportData;

	static uint8_t charSent = 1;

  if(charSent) {
		report->Modifier = 0;
		report->Reserved = 0;
		report->KeyCode[0] = 0;
		charSent = 0;
	} else if(idx < idxEnd) {
		report->Modifier   = sendable[(uint8_t)outstr[idx]][0];
		report->Reserved   = sendable[(uint8_t)outstr[idx]][1];
		report->KeyCode[0] = sendable[(uint8_t)outstr[idx]][2];
		++idx;
		charSent = 1;
  }

	*ReportSize = sizeof(USB_KeyboardReport_Data_t);

	return true;
}

// Called on report input. For keyboard HID devices, thats the state of the LEDs
void CALLBACK_HID_Device_ProcessHIDReport(
						USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
						const uint8_t ReportID,
						const uint8_t ReportType,
						const void* ReportData,
						const uint16_t ReportSize )
{
#ifdef LED_LOCKSTATUS
	uint8_t *report = (uint8_t*)ReportData;

	if(*report & HID_KEYBOARD_LED_NUMLOCK) {
		// numlock is on, ready for input
		// For testing purposes, set breakpoint here to check systick
		XMC_GPIO_SetOutputHigh(NUMLOCK_LED);
	} else {
		// num lock is off, not ready for input, start timediff
		XMC_GPIO_SetOutputLow(NUMLOCK_LED);
	}

	//capslock led flashing; not yet further implemented. Will toggle a state
	if(*report & HID_KEYBOARD_LED_CAPSLOCK) {
		XMC_GPIO_SetOutputHigh(CAPSLOCK_LED);
	} else {
		XMC_GPIO_SetOutputLow(CAPSLOCK_LED);
  }
#endif
}

bool CALLBACK_HIDParser_FilterHIDReportItem(HID_ReportItem_t* const CurrentItem)
{
  return true;
}

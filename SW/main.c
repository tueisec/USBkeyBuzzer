/*
  Copyright 2017 Florian Wilde <florian.wilde@tum.de>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "LUFA/KeyboardHID.h"
#include <xmc_gpio.h>

/* Clock configuration */
XMC_SCU_CLOCK_CONFIG_t clock_config = {
  .syspll_config.p_div  = 2,
  .syspll_config.n_div  = 80,
  .syspll_config.k_div  = 4,
  .syspll_config.mode   = XMC_SCU_CLOCK_SYSPLL_MODE_NORMAL,
  .syspll_config.clksrc = XMC_SCU_CLOCK_SYSPLLCLKSRC_OSCHP,
  .enable_oschp         = true,
  .calibration_mode     = XMC_SCU_CLOCK_FOFI_CALIBRATION_MODE_FACTORY,
  .fsys_clksrc          = XMC_SCU_CLOCK_SYSCLKSRC_PLL,
  .fsys_clkdiv          = 1,
  .fcpu_clkdiv          = 1,
  .fccu_clkdiv          = 1,
  .fperipheral_clkdiv   = 1
};

void SystemCoreClockSetup(void) {
  /* Setup settings for USB clock */
  XMC_SCU_CLOCK_Init(&clock_config);

  XMC_SCU_CLOCK_EnableUsbPll();
  XMC_SCU_CLOCK_StartUsbPll(2, 64);
  XMC_SCU_CLOCK_SetUsbClockDivider(4);
  XMC_SCU_CLOCK_SetUsbClockSource(XMC_SCU_CLOCK_USBCLKSRC_USBPLL);
  XMC_SCU_CLOCK_EnableClock(XMC_SCU_CLOCK_USB);

  SystemCoreClockUpdate();
}

/* GPIO configuration */
const XMC_GPIO_CONFIG_t inH_config = {
  .mode=XMC_GPIO_MODE_INPUT_PULL_UP,
  .output_level=XMC_GPIO_OUTPUT_LEVEL_LOW,
  .output_strength=XMC_GPIO_OUTPUT_STRENGTH_STRONG_SHARP_EDGE
};

#define BUTTON1  XMC_GPIO_PORT1, 14

/* String that is send upon button press */
const char keys[] = "A";
const size_t keysLen = sizeof(keys);

/* Global variables */
volatile size_t systick = 0;
  /* Negative if the button is not pressed, positive if it is pressed */
volatile int32_t buttonFilter = 0;
const int32_t buttonFilterSaturation = 6;

/**
 * Main program entry point. This routine configures the hardware required by
 * the application, then enters a loop to run the application tasks in sequence.
 */
int main(void) {
  uint32_t lastButtonState = 0, currentButtonState = 0;
  SysTick_Config(SystemCoreClock/1000);

  // Init LED pins for debugging and NUM/CAPS visual report
  USB_Init();
  XMC_GPIO_Init(BUTTON1, &inH_config);

  while (1) {
    if(   buttonFilter >= buttonFilterSaturation \
       || buttonFilter <= -buttonFilterSaturation) {
      currentButtonState = buttonFilter > 0;
    }
    if(lastButtonState == 0 && currentButtonState != 0) {
      setHIDoutstr(keys, keysLen);
    }
    lastButtonState = currentButtonState;
    HID_Device_USBTask(&Keyboard_HID_Interface);
  }
}

void SysTick_Handler(void) {
  if(XMC_GPIO_GetInput(BUTTON1)) {
    if(buttonFilter > -buttonFilterSaturation) buttonFilter--;
  } else {
    if(buttonFilter < buttonFilterSaturation) buttonFilter++;
  }
  ++systick;
}
